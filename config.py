import os

worlds["NewWorld"] = os.environ["MINECRAFT_SERVER_PATH"]
outputdir = os.environ["MAP_DESTINATION_PATH"]

def signFilter(poi):
    if poi["id"] == "Sign" or poi["id"] == "minecraft:sign":
        return "\n".join([poi["Text1"], poi["Text2"], poi["Text3"], poi["Text4"]])

def playerFilter(poi):
    if poi["id"] == "Player":
        poi["icon"] = "https://overviewer.org/avatar/%s" % poi["EntityId"]
        return "Last known location for %s" % poi["EntityId"]

def townFilter(poi):
    if poi["id"] == "Town":
        return poi["name"]

def buildingFilter(poi):
    if poi["id"] == "Building":
        return poi["name"]

def landmarksFilter(poi):
    if poi["id"] == "Landmark":
        return poi["name"]

def waterbodiesFilter(poi):
    if poi["id"] == "Waterbodies":
        return poi["name"]

townPois = [
            {"id":"Town",
            "x":1200,
            "y":68,
            "z":-386,
            "name":"Town of Beirut"},
            {"id":"Town",
            "x":532,
            "y":69,
            "z":-278,
            "name":"Town of Salem"},
            {"id":"Town",
            "x":800,
            "y":66,
            "z":-140,
            "name":"Town of Canal"},
            {"id":"Town",
            "x":2404,
            "y":75,
            "z":-1493,
            "name":"Le Village des Inconnus"},
            {"id":"Town",
            "x":2554,
            "y":66,
            "z":-2046,
            "name":"Le Village du Golem"}
            ]

buildingPois = [
            {"id":"Building",
            "x":1078,
            "y":63,
            "z":-139,
            "name":"Fort McGill"},
            {"id":"Building",
            "x":1038,
            "y":73,
            "z":-185,
            "name":"Tower Vision"},
            {"id":"Building",
            "x":1046,
            "y":73,
            "z":-316,
            "name":"Church of Beirut"},
            {"id":"Building",
            "x":1861,
            "y":91,
            "z":-2872,
            "name":"Jungle Temple"},
            {"id":"Building",
            "x":1880,
            "y":91,
            "z":-2281,
            "name":"Jungle Temple"},
            ]

landmarksPois = [
            {"id":"Landmark",
            "x":2158,
            "y":64,
            "z":-2822,
            "name":"Trouble in Little China"},
            {"id":"Landmark",
            "x":1563,
            "y":94,
            "z":-3634,
            "name":"Les Badlands"}
            ]

waterbodiesPois = [
            {"id":"Waterbodies",
            "x":1093,
            "y":63,
            "z":-252,
            "name":"Southern Tuscana Sea"},
            {"id":"Waterbodies",
            "x":681,
            "y":63,
            "z":-510,
            "name":"Northern Tuscana Sea"},
            {"id":"Waterbodies",
            "x":-98,
            "y":63,
            "z":531,
            "name":"Saint-Jean Sea"}
            ]

allPois = townPois + buildingPois + waterbodiesPois

worldMarkers = [dict(name="Signs", filterFunction=signFilter),
                dict(name="Players", filterFunction=playerFilter),
                dict(name="Towns", filterFunction=townFilter, icon="icons/marker_town.png"),
                dict(name="Buildings", filterFunction=buildingFilter),
                dict(name="Landmarks", filterFunction=landmarksFilter),
                dict(name="Waterbodies", filterFunction=waterbodiesFilter)]

renders["survivalday"] = {
    "world": "NewWorld",
    "title": "Daytime",
    "rendermode": smooth_lighting,
    "dimension": "overworld",
    "manualpois": allPois,
    "markers": worldMarkers
}

renders["biomeover"] = {
    "world": "NewWorld",
    "title": "Biomes",
    "rendermode": [ClearBase(), BiomeOverlay()],
    "dimension": "overworld",
    "manualpois": allPois,
    "overlay": ["survivalday"]
}

renders["survivalnight"] = {
    "world": "NewWorld",
    "title": "Nighttime",
    "rendermode": smooth_night,
    "dimension": "overworld",
    "manualpois": allPois,
    "markers": worldMarkers
}

renders["survivalcave"] = {
    "world": "NewWorld",
    "title": "Cavetime",
    "rendermode": cave,
    "dimension": "overworld",
    "manualpois": allPois
}